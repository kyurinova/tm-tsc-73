package ru.tsc.kyurinova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.dto.model.AbstractEntityDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface AbstractDTORepository<E extends AbstractEntityDTO> extends JpaRepository<E, String> {

    void deleteAll();

    @NotNull
    List<E> findAll();

}


