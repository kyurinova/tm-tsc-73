package ru.tsc.kyurinova.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;

@Repository
@Scope("prototype")
public interface ProjectDTORepository extends AbstractOwnerDTORepository<ProjectDTO> {

}
