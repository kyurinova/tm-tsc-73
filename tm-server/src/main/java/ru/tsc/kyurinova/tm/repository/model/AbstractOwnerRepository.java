package ru.tsc.kyurinova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.kyurinova.tm.model.AbstractOwnerModel;

import java.util.List;

@NoRepositoryBean
public interface AbstractOwnerRepository<E extends AbstractOwnerModel> extends AbstractRepository<E> {

    @Nullable
    E findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    E findByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<E> findAllByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndName(@NotNull final String userId, @NotNull final String name);

}
