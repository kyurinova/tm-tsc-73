package ru.tsc.kyurinova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionDTORepository extends AbstractDTORepository<SessionDTO> {

    Optional<SessionDTO> findById(@NotNull String id);

    @Nullable
    List<SessionDTO> findAll();

    void deleteById(@NotNull String id);

    long count();

}
