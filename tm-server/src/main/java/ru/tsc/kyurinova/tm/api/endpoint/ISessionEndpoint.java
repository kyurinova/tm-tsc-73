package ru.tsc.kyurinova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    @NotNull
    SessionDTO openSession(
            @NotNull
            @WebParam(name = "login", partName = "login")
                    String login,
            @NotNull
            @WebParam(name = "password", partName = "password")
                    String password
    );

    @WebMethod
    void closeSession(
            @WebParam(name = "session", partName = "session")
            @NotNull
                    SessionDTO session
    );

}
