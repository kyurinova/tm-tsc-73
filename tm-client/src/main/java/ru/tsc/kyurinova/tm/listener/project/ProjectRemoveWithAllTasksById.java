package ru.tsc.kyurinova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.endpoint.ProjectDTO;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class ProjectRemoveWithAllTasksById extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-remove-with-tasks-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    @EventListener(condition = "@projectRemoveWithAllTasksById.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = projectEndpoint.findByIdProject(sessionService.getSession(), projectId); //serviceLocator.getProjectEndpoint().findByIdProject(session, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        projectEndpoint.removeByIdProject(sessionService.getSession(), projectId);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
