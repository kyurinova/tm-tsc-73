package ru.tsc.kyurinova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.listener.AbstractListener;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

@Component
public class UserChangePasswordListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "change-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "change user's password...";
    }

    @Override
    @EventListener(condition = "@userChangePasswordListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @Nullable final SessionDTO session = sessionService.getSession();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        adminUserEndpoint.setPasswordUser(session, session.getUserId(), password);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
