package ru.tsc.kyurinova.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_role")
public final class Role {

    @Id
    @Column(name = "row_id")
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "role_type")
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}

