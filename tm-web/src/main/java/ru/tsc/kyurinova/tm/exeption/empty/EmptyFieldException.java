package ru.tsc.kyurinova.tm.exeption.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.exeption.AbstractException;

public class EmptyFieldException extends AbstractException {

    public EmptyFieldException(@NotNull String field) {
        super("Error. " + field + " is empty.");
    }

}
