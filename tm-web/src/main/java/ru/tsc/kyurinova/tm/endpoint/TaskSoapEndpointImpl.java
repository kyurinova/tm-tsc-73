package ru.tsc.kyurinova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.kyurinova.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kyurinova.tm.dto.soap.*;
import ru.tsc.kyurinova.tm.util.UserUtil;

@Endpoint
public class TaskSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://kyurinova.tsc.ru/tm/dto/soap";

    @Autowired
    private ITaskDTOService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) throws Exception {
        return new TaskFindAllResponse(taskService.findAllByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllByProjectIdRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskFindAllByProjectIdResponse findAllByProjectId(@RequestPayload final TaskFindAllByProjectIdRequest request) throws Exception {
        return new TaskFindAllByProjectIdResponse(taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), request.getProjectId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskAddRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskAddResponse add(@RequestPayload final TaskAddRequest request) throws Exception {
        return new TaskAddResponse(taskService.addByUserId(UserUtil.getUserId(), request.getTask()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) throws Exception {
        return new TaskSaveResponse(taskService.updateByUserId(UserUtil.getUserId(), request.getTask()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) throws Exception {
        return new TaskFindByIdResponse(taskService.findOneByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskExistsByIdResponse existsById(@RequestPayload final TaskExistsByIdRequest request) throws Exception {
        return new TaskExistsByIdResponse(taskService.existsByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskCountResponse count(@RequestPayload final TaskCountRequest request) throws Exception {
        return new TaskCountResponse(taskService.countByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) throws Exception {
        taskService.removeByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) throws Exception {
        taskService.removeByUserId(UserUtil.getUserId(), request.getTask());
        return new TaskDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDeleteAllResponse clear(@RequestPayload final TaskDeleteAllRequest request) throws Exception {
        taskService.clearByUserId(UserUtil.getUserId());
        return new TaskDeleteAllResponse();
    }

}
