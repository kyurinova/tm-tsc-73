package ru.tsc.kyurinova.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tsc.kyurinova.tm.model.Result;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface AuthEndpoint {

    @WebMethod
    @PostMapping("/login")
    Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    );

    @WebMethod
    @GetMapping("/profile")
    UserDTO profile();

    @WebMethod
    @PostMapping("/logout")
    Result logout();

}

