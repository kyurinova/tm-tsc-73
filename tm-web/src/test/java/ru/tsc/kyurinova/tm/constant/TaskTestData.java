package ru.tsc.kyurinova.tm.constant;

import org.jetbrains.annotations.NotNull;

public class TaskTestData {

    @NotNull
    public final static String TASK1_NAME = "Test Task 1";

    @NotNull
    public final static String TASK1_DESCRIPTION = "Test Task Description 1";

    @NotNull
    public final static String TASK2_NAME = "Test Task 2";

    @NotNull
    public final static String TASK2_DESCRIPTION = "Test Task Description 2";

    @NotNull
    public final static String TASK3_NAME = "Test Task 3";

    @NotNull
    public final static String TASK3_DESCRIPTION = "Test Task Description 3";

}
